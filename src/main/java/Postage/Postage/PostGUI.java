package Postage.Postage;

import javax.swing.*;

import java.awt.event.*;
import java.io.FileReader;
import java.io.IOException;


public class PostGUI extends JFrame implements ActionListener {
	
	private JLabel Weight, Height, Length, Value, Delivery, Depth; 
	private JTextField Weightbox, Heightbox, Lengthbox, Valuebox, Recommendbox, Depthbox, DeliveryDesired;
	private JButton Recommend;
	private JPanel top, bottom;
	private String Service; 
	private int height, weight, length, depth, delivery, value, parcelsize;

	public PostGUI() {
		
		//Creates GUI for post Mailing recommendations 
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(380,200);
		setLocation(100,100);
		setTitle("Mailing recommendations");
		
		this.Layout();
	
		Recommend.addActionListener(this);
		
	}
	private void Layout() {
		//Creates GUI Layout 
		
		//Creates top/main panel for GUI 
		JPanel top = new JPanel();
		this.add(top);
		
		//Creates weight label and text field and adds to GUI
		Weight = new JLabel("Weight");
		top.add(Weight);
		Weightbox = new JTextField(10);
		top.add(Weightbox);
			
		//Creates height label and text field and adds to GUI
		Height = new JLabel("Height");
		top.add(Height);
		Heightbox = new JTextField(10);
		top.add(Heightbox);
		
		//Creates length label and text field and adds to GUI
		Length = new JLabel("Length");
		top.add(Length);
		Lengthbox = new JTextField(10);
		top.add(Lengthbox);
		
		//Creates depth label and text field and adds to GUI
		Depth = new JLabel("Depth");
		top.add(Depth);
		Depthbox = new JTextField(10);
		top.add(Depthbox);
		
		//Creates value label and text field and adds to GUI
		Value = new JLabel("Value");
		top.add(Value);
		Valuebox = new JTextField(10);
		top.add(Valuebox);
		
		//Creates Delivery label and text field and adds to GUI
		Delivery = new JLabel("Delivery 1 - 3 days");
		top.add(Delivery);	
		DeliveryDesired = new JTextField(10);
		top.add(DeliveryDesired);
		
		
		//Creates Recommend button and adds to GUI
		Recommend = new JButton("Recommendation");	
		top.add(Recommend);
		
		//Creates Recommend text field and sets to non-editable
		//Adds Recommend text box to GUI
		Recommendbox = new JTextField(20);
		Recommendbox.setEditable(false);
		top.add(Recommendbox);
			
	}
	public void actionPerformed(ActionEvent e) {
		/* When Recommend button is pressed, values from each text field are retrieved 
		 * and Service method is called with the values from the text fields as parameters 
		 */
		if(e.getSource() == Recommend) {
			
			//Gets values from text fields and parses them to integer values 
			String Strweight = Weightbox.getText();
			weight = Integer.parseInt(Strweight);
			
			String Strheight = Heightbox.getText();
			height = Integer.parseInt(Strheight);
			
			String Strlength = Lengthbox.getText();
			length = Integer.parseInt(Strlength);
			
			String Strdepth = Depthbox.getText();
			depth = Integer.parseInt(Strdepth);
			
			String StrDelivery = DeliveryDesired.getText();
			delivery = Integer.parseInt(StrDelivery);
			
			String StrValue = Valuebox.getText();
			value = Integer.parseInt(StrValue);
			
			//Calls Service method 
			Service(height, length, depth, delivery, value, weight); 
			
			//Sets the text field recommend box to the new value of service and updates the display
			Recommendbox.setText(Service);
			Recommendbox.repaint();
		}
	}
	public String Service(int ParHeight, int ParLength, int ParDepth, int Pardeliveryspeed, int Parvalue, int Parweight) {
		//Calculates service based on parcel size, desired deliveryspeed and value 
		parcelsize = ParHeight + ParLength + ParDepth;
		
		//If parcel size is over 90, or weight is over 2000 service will be parecelforce 
		if (parcelsize >= 90 || Parweight >= 2000) {
			Service = "ParcelForce";
			//If parcel value is over 50 or deliveryspeed is next day service will be special
		} else if (Parvalue >= 50 || Pardeliveryspeed == 1) {
			Service = "Special";
			//If parcel value is over 20 service and deliveryspeed is 1-2 days service will be first class signed for 
		} else if (Parvalue >= 20 && Pardeliveryspeed == 2) {
			Service = "First class Signed for";
			//If parcel value is over 20 and delivery speed is 2-3 days service will be second class signed for 
		} else if (Parvalue >= 20 && Pardeliveryspeed == 3) {
			Service = "Second class Signed for";
		//If parcel value is under 20 and delivery speed is 1 - 2 days service will be first class 
		} else if (Parvalue <= 19 && Pardeliveryspeed == 2) {
			Service = "First class";
		 //If parcel value is under 20 and delivery speed is 2-3 days service will be second class 
		} else 
			Service = "Second class";
		
	
	   
	    
	    //Returns value of Service
	    return Service;
	}

}
